﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KrutFrut.Models;

namespace KrutFrut.Controllers
{
    public class HomeController : Controller
    {
        PeopleContext db = new PeopleContext();

        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public string Add(People p)
        {
            db.Human.Add(p);
            db.SaveChanges();
            return "Друг "+ p.name +" добавлен";
            
        }

        [HttpGet]
        public ActionResult Search()
        {
            var p = db.Human;
            ViewBag.Human = p;
            return View();
        }

        [HttpGet]
        public ActionResult Delete()
        {
            return View();
        }

        [HttpPost]
        public string Delete(People p)
        {
            foreach(var c in db.Human) 
            {
                int i = 0;
                if (c.name == p.name && c.date == p.date) 
                {
                    i += 1;
                    db.Human.Remove(p);
                }

                if (i == 0) { return "Друга с такими данными у вас нет: " + p.name ; }

            }
            db.SaveChanges();
            return "Друг " + p.name + "удален";
        }
    }
}