﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KrutFrut.Models
{
    public class People
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime date { get; set; }
    }
}