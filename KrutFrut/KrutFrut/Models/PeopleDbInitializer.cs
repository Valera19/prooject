﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace KrutFrut.Models
{
    public class PeopleDbInitializer: DropCreateDatabaseAlways<PeopleContext>
    {
        protected override void Seed(PeopleContext db)
        {
            db.Human.Add(new People {name="Валерий",date = new DateTime(2003, 11, 1) });
            db.Human.Add(new People { name = "Аня", date = new DateTime(2000, 1, 12) });
            db.Human.Add(new People { name = "Катя", date = new DateTime(2001, 5, 26) });
            base.Seed(db);
        }
    }
}